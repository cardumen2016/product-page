var gulp = require('gulp'),
	rename = require('gulp-rename'),
	pug = require('gulp-pug'),
	data = require('gulp-data'),
	fs = require('fs'),
	excel2json = require('gulp-excel2json'),

	argv = require('yargs').argv;

var _file;
gulp.task('default', function() {
	// console.log();
	_endCol = argv.endCol;
	_file = argv.file;
	// console.log(_endCol);
	// console.log(_file);
	// return false;
	// place code for your default task here
	gulp.src('data/' + _file + '.xlsx')
		.pipe(excel2json({
			headRow: 3,
			valueRowStart: 4,
			trace: true,
			endCol: _endCol
		})
		.on('error', function(e){
			console.log(e);
			console.log("error");
		})
		.on('end', function(e){
			console.log('Ending');
			cfg = {
				file: _file
			}
			// readJSON(cfg);
			gulp.start('readJSON');
		}))
		.pipe(rename("data.json"))
		.pipe(gulp.dest('build/'+_file))
});

gulp.task('pug', function(){
	return gulp.src('./templates/**/*.pug')
	.pipe(data(function() {
			return JSON.parse(fs.readFileSync('./build/data.json'))
		}))
	.pipe(pug({
		pretty: true
	}).on('error', function(e){
		// console.log(e);
		console.log(["Message -> ", e.message]);
		console.log(["Plugin ->", e.plugin]);
	}).on('end', function(e){
		console.log('Ending');
		// console.log(e);
	}))
	.pipe(gulp.dest('./html'));
});

gulp.task('readJSON', function(){
	console.log('readJSON -> ' + _file);
	var json = JSON.parse(fs.readFileSync('./build/' + _file + '/data.json'));
	var data = json.data;

	for(x in data) {
		params = {
			dir: _file,
			obj: data[x]
		};
		pug2(params);
	}
	
	// populate(usuarios);
});


// FUNCTIONS
var readJSON = function(_params) {
	console.log(_params);
	gulp.task('readJSON', function(){
		var json = JSON.parse(fs.readFileSync('./build/' + _params.file + '/data.json'));
		var data = json.data;

		for(x in data) {
			params = {
				dir: _file,
				obj: data[x]
			};
			pug2(params);
		}
	});
}

var pug2 = function(_params) {
	// console.log(obj);

	obj = _params.obj;
	gulp.src('./templates/' + _params.dir + '/index.pug')
	.pipe(pug({
		pretty: true,
		data: obj,
	}).on('error', function(e){
		// console.log(e);
		console.log(["Message -> ", e.message]);
		console.log(["Plugin ->", e.plugin]);
	}).on('end', function(e){
		console.log('HTML Generado '+ _params.dir + '/' + obj.sku + '.html');
		// console.log(e);
	}))
	.pipe(rename(obj.sku+".html"))
	.pipe(gulp.dest('./html/'+_params.dir));
}


const XlsxPopulate = require('xlsx-populate');

var populate = function(obj) {
	// Load a new blank workbook
	XlsxPopulate.fromFileAsync('./data/Book1.xlsx')
	.then(workbook => {
			for(x in obj) {
				var _html = fs.readFileSync('./html/' + obj[x].id + '.html', 'utf8');
				console.log(_html);
				row = obj[x].id + 1;
				// Modify the workbook.
				workbook.sheet("Sheet1").cell("D" + row).value(_html);

				// Write to file.
				workbook.toFileAsync("./data/Book1.xlsx");
			}
		}
	);
}