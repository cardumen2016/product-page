'use strict';
var gutil = require('gulp-util');
var through = require('through2');
var XLSX = require('xlsx');

/**
 * excel filename or workbook to json
 * @param fileName
 * @param headRow
 * @param valueRow
 * @returns {{}} json
 */
var toJson = function (fileName, headRow, valueRow, endCol) {
    var workbook;
    if (typeof fileName === 'string') {
        workbook = XLSX.readFile(fileName);
    } else {
        workbook = fileName;
    }
    var worksheet = workbook.Sheets[workbook.SheetNames[0]];
    var namemap = [];

    // json to return
    var json = [];
    var curRow = 0;
    var fila = {}
    var main = {};
    console.log('endCol', endCol);
    for (var key in worksheet) {
        if (worksheet.hasOwnProperty(key)) {
            var cell = worksheet[key];
            var match = /([A-Z]+)(\d+)/.exec(key);
            if (!match) {
                // console.log('Fin fila');
                continue;
            }
            // console.log(match);
            // console.log(match.length);
            var col = match[1]; // ABCD
            var row = match[2]; // 1234
            var value = cell.v;
            // console.log(col, row, value);
            if (row == headRow) {
                namemap[col] = value;
            } else if (row < valueRow) {
                //continue;
            } else {
                /*
                if (col == "A") {
                    json[cell.v] = {};
                    curRow = cell.v;
                }
                json[curRow][namemap[col]] = cell.v;
                */
                fila[namemap[col]] = cell.v;

                if (col == endCol) {
                    json.push(fila);
                    fila = {};
                }
            }
        }
    }
    main['data'] = json;
    return main;
};


module.exports = function (options) {
    options = options || {};
    return through.obj(function (file, enc, cb) {
        if (file.isNull()) {
            this.push(file);
            return cb();
        }

        if (file.isStream()) {
            this.emit('error', new gutil.PluginError(PLUGIN_NAME, 'Streaming not supported'));
            return cb();
        }

        var arr = [];
        for (var i = 0; i < file.contents.length; ++i) arr[i] = String.fromCharCode(file.contents[i]);
        var bString = arr.join("");

        /* Call XLSX */
        var workbook = XLSX.read(bString, {type: "binary"});
        file.contents = new Buffer(JSON.stringify(toJson(workbook, options.headRow || 1, options.valueRowStart || 2, options.endCol || 'E')));

        if (options.trace) {
            console.log("convert file :" + file.path);
        }

        this.push(file);
        cb();
    });
};
